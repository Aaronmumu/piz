<?php
/**
 * APP配置
 */
return [
    'name'      => '我是大奸商',                          //项目名称
    'namespace' => 'app',                               //项目命名空间
    'path'      => realpath (__DIR__.'/../app/'),  //项目所在路径
    'gzip'      => 0,                                   //gzip 等级， 请查看  https://wiki.swoole.com/wiki/page/410.html
    //server设置
    'ip'        => '0.0.0.0',   //监听IP
    'port'      => 9501,        //监听端口
    'server'    => 'websocket' ,//服务，可选 websocket 默认http
    'set'       => [            //配置参数  请查看  https://wiki.swoole.com/wiki/page/274.html
        'daemonize'             => 1 ,
        'enable_static_handler' => 0 ,
        'document_root'         => realpath (__DIR__.'/../static/') ,

        'worker_num'            => 2,
        'max_request'		    => 100000,
        'task_worker_num'       => 2,

        'reactor_num'           => 2,
        'dispatch_mode'         => 2,
        'task_max_request'      => 600,
        'task_ipc_mode'         => 3,
        'open_length_check'     => true,
        'package_length_type'   => 'n',
        'package_length_offset' => 0,
        'package_body_offset'   => 2,
        'package_max_length'    => 80000,
        'message_queue_key'     => 12001 + (TSWOOLE_SID - 1) * 50
    ],
    //监控配置
    'monitor' =>  [
        'timer'     =>  3000 ,  //定时器间隔时间，单位毫秒
        'restart'   => 1 ,       //重启
    ] ,
    //日志
    'log' => [
        //输出到屏幕，当 set.daemonize = false 时，该配置生效，
        'echo'  => 0 ,
        // 日志保存目录
        'path'  => LOG_PATH,
        // 日志记录级别，共8个级别
        'level' => ['EMERGENCY','ALERT','CRITICAL','ERROR','WARNING','NOTICE','INFO','DEBUG','SQL'] ,
    ] ,
];
