<?php
/**
 * 数据库配置
 */
return [
    'default' => [
        'hostname' => 'localhost',		//服务器地址
        'port' => 3306,				    //数据库连接端口
        'database' => 'test',			// 数据库名
        'username' => 'root',			// 数据库用户名
        'password' => 'root123',        // 数据库密码
        'charset' => 'utf8',			// 数据库编码默认采用utf8
        'debug' => true,                // 调试模式
        'pconnect' => true,			    // 长连接
    ],
];