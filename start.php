<?php
require "./frame/base.php";

use Piz\Log;

try {
    Log::get_instance()->write('ERROR', '小宝贝，跑起来');
    start::run( isset($argv[1]) ? $argv[1] : '');
} catch (Exception $e) {
    Log::get_instance()->write('ERROR', $e);
}
