<?php

namespace Piz;

class Hook
{
    private static $instance;
    private static $config;

    private function __construct()
    {
    }

    public static function get_instance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
            self::$config = Config::get_instance()->get("hook");
        }
        return self::$instance;
    }

    public function listen($hook, ...$args)
    {
        $hooks = isset(self::$config[$hook]) ? self::$config[$hook] : [];
        while ($hooks) {
            list($class, $func) = array_shift($hooks);
            try {
                $class::get_instance()->$func(...$args);
            } catch (\Exception $e) {
                Log::get_instance()->write('ERROR', $e->getMessage());
            }
        }
    }
}